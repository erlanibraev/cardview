package kz.test

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.test.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class CardViewTest {

    val histogram = intArrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 3, 1, 1, 2, 4, 5, 4, 8, 7, 6, 11, 63, 1, 1, 1, 10, 1, 0, 1, 1, 0, 2, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 2, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 2, 0, 0, 2, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 2, 1, 89, 0, 0, 0)

    @Test
    fun readTest() {
        val result = CardViewTest::class.java.classLoader.getResource("TEST01.png").readImage()
        println("${result.height} x ${result.width}")
        assertEquals(result.height, 1166)
        assertEquals(result.width, 636)
    }

    @Test
    fun readFailTest() {
        assertThrows<Exception> {
            CardViewTest::class.java.classLoader.getResource("unnamed.png").readImage()
        }
    }

    @Test
    fun readCardPlace() {
        CardViewTest::class.java.classLoader.getResource("TEST01.png")
                .readImage()
                .getCardPlace().forEachIndexed { index, bufferedImage ->
                    ImageIO.write(bufferedImage, "png", File("card$index.png"))
                }
    }

    @Test
    fun isCardTest() {
        assert(CardViewTest::class.java.classLoader.getResource("card0.png").readImage().isCard())
    }

    @Test
    fun isDarkCardTest() {
        assert(CardViewTest::class.java.classLoader.getResource("card1.png").readImage().isCard())
    }

    @Test
    fun isNotCard() {
        assert(!CardViewTest::class.java.classLoader.getResource("card4.png").readImage().isCard())
    }

    @Test
    fun readCardTest() {
        val result1 = CardViewTest::class.java.classLoader.getResource("TEST01.png")
                .readImage()
                .getCardPlace()
                .getCard()
        assertEquals(result1.size, 5)

        val result2 = CardViewTest::class.java.classLoader.getResource("TEST02.png")
                .readImage()
                .getCardPlace()
                .getCard()
        assertEquals(result2.size, 3)

        val result3 = CardViewTest::class.java.classLoader.getResource("TEST03.png")
                .readImage()
                .getCardPlace()
                .getCard()
        assertEquals(result3.size, 4)
    }

    @Test
    fun getRankImageTest() {
        val template = readTemplate("5.png")
        readTemplate("card0.png")
                .getRankImage(OFFSET_OF_CARD_RANK_X, template.width, template.height)
                .apply {
                    ImageIO.write(this, "png", File("rank.png"))
                }
    }

    @Test
    fun getRankByKeyImageTest() {
        val template = readTemplate("A_7.png")
        readTemplate("card2.png")
            .getRankByKeyImage("A_7", template.width, template.height).apply {
                ImageIO.write(this, "png", File("A_R_RANK.png"))
            }
    }

    @Test
    fun getSuitsImageTest() {
        val template = readTemplate("h.png")
        readTemplate("card0.png")
                .getSuitsImage(template.width, template.height)
                .apply {
                    ImageIO.write(this, "png", File("suits.png"))
                }

    }

    @Test
    fun testIsRank() {
        val template = readTemplate("5.png")
        assert(
                readTemplate("rank.png")
                        .isRank(template)
        )
    }

    @Test
    fun testNotIsRank() {
        val template = readTemplate("4.png")
        assertFalse(
            readTemplate("rank.png")
                .isRank(template)
        )
    }

    @Test
    fun testIsSuits() {
        val template = readTemplate("s.png")
        assert(
            readTemplate("suits.png").isSuits(template)
        )
    }

    @Test
    fun testNotIsSuits() {
        val template = readTemplate("h.png")
        assertFalse(
            readTemplate("suits.png").isSuits(template)
        )
    }

    @Test
    fun getCompareIndexOfSubImagesTest() {
        val template = readTemplate("5.png")

        val img = readTemplate("rank.png")
        val result = getCompareIndexOfSubImages(img, template)
        println(result)
        assert(result <= ALLOW_RANK_SCALE)
    }

    @Test
    fun getDifferencePercentTest() {
        val template = readTemplate("5.png")
        val img = readTemplate("rank.png")

        ImageIO.write(img.grayscale(), "png", File("grayscale_rank.png"))
        val binarize = img.binarize()
        ImageIO.write(binarize, "png", File("binarize_rank.png"))
        val changeColor = binarize.changeColor(ARRAY_RGB_OF_SHADOW_CARD_SURFACE)

        ImageIO.write(changeColor, "png", File("change_color_rank.png"))

        val result = getDifferencePercent(changeColor, template)

        println(result)

        assert(result <= ALLOW_RANK_SCALE.toDouble())
    }

    @Test
    fun grayscaleTest() {
        val img = readTemplate("rank.png")
        val grayscale = readTemplate("grayscale_rank_test.png")
        val result = img.grayscale()
        ImageIO.write(result, "png", File("grayscale_rank1.png"))
        assert(grayscale.equalsImage(result))
    }

    @Test
    fun binarizeTest() {
        val img = readTemplate("rank.png")
        val binarize = readTemplate("binarize_rank_test.png")
        val result = img.binarize()
        ImageIO.write(result, "png", File("binarize_rank1.png"))
        assert(binarize.equalsImage(result))
    }

    @Test
    fun colorToRGBTest() {
        val result = colorToRGB(255, 205, 205, 205)
        assertEquals(result, -3289651)
    }

    @Test
    fun tresholdTest() {
        val grayscale = readTemplate("grayscale_rank_test.png")
        val result = grayscale.treshold()
        assertEquals(134, result)
    }

    @Test
    fun getHistogramTest() {
        val grayscale = readTemplate("grayscale_rank_test.png")
        val result = grayscale.getHistogram().toCollection(arrayListOf())
        println(result)
        println(histogram.toCollection(arrayListOf()))
        println(histogram.equals(result))
        assertEquals(result,histogram.toCollection(arrayListOf()))
    }

    @ParameterizedTest
    @CsvSource(
        "card0.png,5",
        "card1.png,4",
        "card2.png,A",
    )
    fun getRankNameTest(fileName: String, goal: String) {
        val card = readTemplate(fileName)
        val result = card.getRankName()
        assertEquals(goal, result)
    }

    @Test
    fun isA7Rank() {
        val template = readTemplate("A_7.png")
        val card = readTemplate("card2.png")



        val rank = card.getRankByKeyImage("A", template.width, template.height)

        ImageIO.write(rank, "png", File("A_7_rank.png"))
        ImageIO.write(rank.grayscale(), "png", File("A_7_rank_grayscale.png"))
        ImageIO.write(rank.binarize(), "png", File("A_7_rank_binarize.png"))

        val result = rank.isRank(template)
        println(getCompareIndexOfSubImages(rank, template))
        println(result)
        assert(result)
    }

    @ParameterizedTest
    @CsvSource(
        "card0.png,s",
        "card1.png,h",
        "card2.png,h",
    )
    fun getSuitsNameTest(fileName: String, goal: String) {
        val card = readTemplate(fileName)
        val result = card.getSuitsName()
        println(result)
        assertEquals(goal, result)
    }

    @ParameterizedTest
    @CsvSource(
        "card0.png,5s",
        "card1.png,4h",
        "card2.png,Ah",
    )
    fun getCardNameTest(fileName: String, goal: String) {
        val card = readTemplate(fileName)
        val result = card.getCardName()
        println(result)
        assertEquals(goal, result)
    }

    @ParameterizedTest
    @CsvSource(
        "TEST01.png,JsAh10sQsKc",
        "TEST02.png,5s8d9h",
        "TEST03.png,6h4h9sQc",
    )
    fun getSetNameTest(fileName: String, goal: String) {
        val card = readTemplate(fileName)
        val result = card.getSetName()
        println(result)
        assertEquals(goal, result)
    }

    @Test
    fun scanDirTest() {
        val result = scanDir("images")
        assertEquals(361, result.size)
    }

    @Test
    fun getResultTest() {
        val result = scanDir("images").getResult()
        println(result)
    }



}

fun BufferedImage.equalsImage(img: BufferedImage): Boolean {
    val source = this
    if (source.width != img.width || source.height != img.height) {
        return false
    }
    for (x in 1 until source.width) {
        for (y in 1 until source.height) {
            if (source.getRGB(x, y) != img.getRGB(x, y)) {
                return false
            }
        }
    }
    return true
}





