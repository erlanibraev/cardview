package kz.test

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.net.URL
import javax.imageio.ImageIO


const val IMAGE_WIDTH = 636
const val IMAGE_HEIGHT = 1166
const val CARD_HEIGHT = 88
const val CARD_WIDTH = 63
const val START_Y = 592
const val OFFSET_OF_CARD_RANK_X = 11
const val OFFSET_OF_CARD_SUITS_X = 28
const val OFFSET_OF_CARD_SUITS_Y = 42

const val ALLOW_RANK_SCALE = 17
const val ALLOW_SUITS_SCALE = 7

val CARD_COORD = arrayOf(142, 214, 285, 357, 428)

val white = Color(255, 255, 255)
val gray = Color(120, 120, 120)

val ARRAY_RGB_OF_SHADOW_CARD_SURFACE = intArrayOf(120, 120, 120)
val ARRAY_RGB_OF_NORMAL_CARD_SURFACE = intArrayOf(255, 255, 255)

val RANK_TEMPLATES: Map<String, BufferedImage> = mapOf(
        "2" to readTemplate("2.png"),
        "3" to readTemplate("3.png"),
        "4" to readTemplate("4.png"),
        "5" to readTemplate("5.png"),
        "6" to readTemplate("6.png"),
        "7" to readTemplate("7.png"),
        "8" to readTemplate("8.png"),
        "9" to readTemplate("9.png"),
        "10" to readTemplate("10.png"),
        "A" to readTemplate("A_7.png"),
        "J" to readTemplate("J.png"),
        "K" to readTemplate("K.png"),
        "Q" to readTemplate("Q.png")
)

val SUITS_TEMPLATES: Map<String, BufferedImage> = mapOf(
        "c" to readTemplate("c.png"),
        "d" to readTemplate("d.png"),
        "h" to readTemplate("h.png"),
        "s" to readTemplate("s.png")
)


fun readTemplate(fileName: String): BufferedImage =
        object: Any() {}.javaClass.classLoader.getResource(fileName)?.let { url ->
            ImageIO.read(url)
        } ?: throw Exception("Шаблон не найден ${fileName}")


fun URL.readImage(): BufferedImage =
        ImageIO.read(this).apply {
            if( (this.width != IMAGE_WIDTH || this.height != IMAGE_HEIGHT)
                    && (this.width != CARD_WIDTH || this.height != CARD_HEIGHT)
            ) {
                throw Exception("Не верный размер изображения ${this@readImage.file}")
            }
        }

fun BufferedImage.getCardPlace(): List<BufferedImage> =
        (0..4).map { i ->
            this.getSubimage(CARD_COORD[i], START_Y, CARD_WIDTH, CARD_HEIGHT)
        }

fun List<BufferedImage>.getCard() =
        this.filter { bufferedImage -> bufferedImage.isCard() }

fun BufferedImage.isCard(): Boolean =
        if(this.width == CARD_WIDTH && this.height == CARD_HEIGHT) {
            val rgb = this.getRGB(CARD_WIDTH / 2, CARD_HEIGHT / 2)
            Color(rgb).equals(white) ||
                    Color(rgb).equals(gray)
        } else {
            false
        }

fun BufferedImage.changeColor(rgb1: IntArray): BufferedImage =
        this.apply {
            val w = this.width
            val h = this.height
            val MASK = 0x00ffffff
            val sourceRgb = rgb1[0] shl 16 or rgb1[1] shl 8 or rgb1[2]
            val destRgb = sourceRgb xor ( rgb1[0] shl 16 or rgb1[1] shl 8 or rgb1[2] )
            val arrRgb = this.getRGB(0, 0, w, h, null, 0, w).apply {
                this.map { i ->
                    if((i and MASK) == sourceRgb) {
                        i xor destRgb
                    } else {
                        i
                    }
                }
            }
            this.setRGB(0, 0, w, h, arrRgb, 0, w)
        }

fun BufferedImage.grayscale(): BufferedImage =
        BufferedImage(this.width, this.height, this.type).apply {
            val img = this@grayscale
            for( i in 0 until  img.width ) {
                for(j in 0 until img.height) {
                    val alpha = Color(img.getRGB(i, j)).alpha
                    val red = Color(img.getRGB(i, j)).red
                    val green = Color(img.getRGB(i, j)).green
                    val blue = Color(img.getRGB(i, j)).blue
                    val newRed = (0.21 * red + 0.71 * green + 0.07 * blue).toInt()
                    val newPixel = colorToRGB(alpha, newRed, newRed, newRed)
                    this.setRGB(i, j, newPixel)
                }
            }
        }

fun colorToRGB(alpha: Int, red: Int, green: Int, blue: Int): Int {
    var newPixel = 0
    newPixel += alpha
    newPixel = newPixel shl 8
    newPixel += red
    newPixel = newPixel shl 8
    newPixel += green
    newPixel = newPixel shl 8
    newPixel += blue
    return newPixel
}

fun BufferedImage.binarize(): BufferedImage =
        BufferedImage(this.width, this.height, this.type).apply {
            val img = this@binarize.grayscale()
            val threshold = img.treshold()

            for(i in 0 until img.width) {
                for( j in 0 until img.height) {
                    val red = Color(img.getRGB(i, j)).red
                    val alpha = Color(img.getRGB(i, j)).alpha
                    val newRed: Int = if(red > threshold) {
                        255
                    } else {
                        0
                    }
                    val newPixel = colorToRGB(alpha, newRed, newRed, newRed)
                    this.setRGB(i, j, newPixel)
                }
            }
        }

fun BufferedImage.treshold(): Int  {
    val img = this
    val histogram = img.getHistogram()
    val total = img.width * img.height
    val sum: Float =
            histogram.foldIndexed<Float>(0.0F) { index, acc, i ->
                acc + (index * i)
            }

    var result = 0;
    var sumB: Float = 0.0F
    var wB = 0
    var wF = 0
    var varMax: Float = 0.0F
    for(i in 0 until 256) {
        wB += histogram[i]
        if(wB == 0) {
            continue
        }

        wF = total - wB

        if(wF == 0) {
            break
        }

        sumB += (i * histogram[i]).toFloat()

        val mB: Float = sumB / wB
        val mF: Float = (sum - sumB) / wF

        val varBetween: Float = wB * wF * (mB - mF) * (mB - mF)

        if(varBetween > varMax) {
            varMax = varBetween
            result = i
        }
    }

    return result
}

fun BufferedImage.getHistogram(): IntArray =
    (0..255)
            .map { i -> 0 }
            .toIntArray()
            .apply {
                val img = this@getHistogram
                for(i in 0..img.width -1) {
                    for(j in 0..img.height -1 ) {
                        val red = Color(img.getRGB(i, j)).red
                        this[red]++
                    }
                }
            }

fun getDifferencePercent(img1: BufferedImage, img2: BufferedImage): Double {
    val w1 = img1.width
    val h1 = img1.height
    var diff: Long = 0
    for (y in 0 until h1) {
        for (x in 0 until w1) {
            diff += getPixelDifferenc(img1.getRGB(x, y), img2.getRGB(x, y)).toLong()
        }
    }
    val totalPixels = 3L * 255 * w1 * h1
    return 100.0 * diff / totalPixels
}

private fun getPixelDifferenc(rgb1: Int, rgb2: Int): Int {
    val r1 = rgb1 shr 16 and 0xff
    val g1 = rgb1 shr 8 and 0xff
    val b1 = rgb1 and 0xff
    val r2 = rgb2 shr 16 and 0xff
    val g2 = rgb2 shr 8 and 0xff
    val b2 = rgb2 and 0xff
    return Math.abs(r1 - r2) + Math.abs(g1 - g2) + Math.abs(b1 - b2)
}

fun getCompareIndexOfSubImages(img11: BufferedImage,
                               img2: BufferedImage): Int {

    return getDifferencePercent(
            img11.binarize().changeColor(ARRAY_RGB_OF_SHADOW_CARD_SURFACE),
            img2).toInt()
}

fun BufferedImage.getRankImage(offset: Int, width: Int, height: Int): BufferedImage =
        this.getSubimage(
                offset, 0, width, height
        )

fun BufferedImage.getRankByKeyImage(key: String, width: Int, height: Int): BufferedImage =
    this.getRankImage(
        if(key == "A") {
            6
        } else {
            OFFSET_OF_CARD_RANK_X
        },
        width,
        height
    )

fun BufferedImage.getSuitsImage(width: Int, height: Int): BufferedImage =
        this.getSubimage(
                OFFSET_OF_CARD_SUITS_X, OFFSET_OF_CARD_SUITS_Y, width, height
        )

fun BufferedImage.isRank(template: BufferedImage): Boolean =
        getCompareIndexOfSubImages(this, template) <= ALLOW_RANK_SCALE

fun BufferedImage.isSuits(template: BufferedImage): Boolean =
        getCompareIndexOfSubImages(this, template) <= ALLOW_SUITS_SCALE

fun BufferedImage.getRankName(): String =
    RANK_TEMPLATES.filter { entry ->
        this@getRankName
            .getRankByKeyImage(entry.key, entry.value.width, entry.value.height)
            .isRank(entry.value)
    }.map { entry ->
        entry.key
    }.fold("?") { acc, s ->
        s
    }

fun BufferedImage.getSuitsName(): String =
    SUITS_TEMPLATES.filter { entry ->
        this@getSuitsName
            .getSuitsImage(entry.value.width, entry.value.height)
            .isSuits(entry.value)
    }.map { entry ->
        entry.key
    }.fold("?") { acc, s ->
        s
    }

fun BufferedImage.getCardName(): String =
    "${this.getRankName()}${getSuitsName()}"

fun BufferedImage.getSetName(): String =
    this
        .getCardPlace()
        .getCard()
        .map { img ->
            img.getCardName()
        }.joinToString(separator = "")

fun scanDir(path: String): Map<String, BufferedImage> =
    File(path).listFiles { d, n ->
        n.endsWith(".png")
    }.map { file ->
        file.name to ImageIO.read(file)
    }.toMap()

fun Map<String, BufferedImage>.getResult():Map<String, String> =
    this.map { entry ->
        entry.key to entry.value.getSetName()
    }.toMap()

fun main(args: Array<String>) {

    if(args.size < 1) {
        println("Укажите путь к картинкам")
        return
    }
    println("Старт")
    File(args[0]).listFiles { d, n ->
        n.endsWith(".png")
    }.forEach { file ->
        try {
            val name = file.name
            val img = ImageIO.read(file)
            println("${name} - ${img.getSetName()}")
        } catch (ex: Exception) {

        }
    }
    println("Финиш")

}